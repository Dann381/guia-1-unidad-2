import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from dlg_advertencia import DlgWindow
from rst_window import RstWindow


class MainWindow():
    

    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./glade/glade.ui")
        
        window = builder.get_object("mainwindow")
        window.connect("destroy", Gtk.main_quit)
        window.set_title("Ventana Principal")
        window.resize(800, 600)
        
        self.entry1 = builder.get_object("entry1")
        self.entry2 = builder.get_object("entry2")
        label1 = builder.get_object("label1")
        label2 = builder.get_object("label2")
        titulo = builder.get_object("titulo")

        self.entry1.connect("changed", self.cambio)
        self.entry2.connect("changed", self.cambio)
        
        reset = builder.get_object("reset")
        reset.connect("clicked", self.btn_reset)

        aceptar = builder.get_object("aceptar")
        aceptar.connect("clicked", self.btn_aceptar)
        window.show_all()


    def cambio(self, entry):
        pass

    
    def btn_aceptar(self, btn=None):
        self.primero = self.entry1.get_text()
        self.segundo = self.entry2.get_text()
        if self.primero.isdigit() == True and self.segundo.isdigit() == True:
            self.primero = int(self.primero)
            self.segundo = int(self.segundo)

        self.texto = self.primero + self.segundo
        dlg = DlgWindow(self.primero, self.segundo, self.texto)
        
    
    def btn_reset(self, btn=None):
        rst = RstWindow()
        response = rst.reset.run()
        if response == Gtk.ResponseType.CANCEL:
            rst.reset.destroy()
        elif response == Gtk.ResponseType.ACCEPT:
            self.entry1.set_text("")
            self.entry2.set_text("")
            rst.reset.destroy()
            
       
        

if __name__ == "__main__":
    MainWindow()
    Gtk.main()
