import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class DlgWindow():


    def __init__(self, primero, segundo, texto):
        builder = Gtk.Builder()
        builder.add_from_file("./glade/glade.ui")
        
        self.dialogo = builder.get_object("dlg_window")
        self.dialogo.set_default_size(300, 200)
        self.dialogo.set_title("Resultados")

        aceptar = builder.get_object("btn_aceptar")
        aceptar.connect("clicked", self.aceptar_click)
        
        self.textouno = builder.get_object("uno")
        self.textodos = builder.get_object("dos")
        self.resultado = builder.get_object("resultado")

        self.labeluno = builder.get_object("labeluno")
        self.labeldos = builder.get_object("labeldos")
        self.labelresultado = builder.get_object("labeltres")
        
        if type(primero) == int:
            primero = str(primero)
        if type(segundo) == int:
            segundo = str(segundo)
        if type(texto) == int:
            texto = str(texto)
        self.labeluno.set_text(primero)
        self.labeldos.set_text(segundo)
        self.labelresultado.set_text(texto)
         
        self.dialogo.show_all()
        
    
    def aceptar_click(self, btn=None):
        self.dialogo.hide()

