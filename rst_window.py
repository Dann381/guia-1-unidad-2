import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class RstWindow():

    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./glade/glade.ui") 
        self.reset = builder.get_object("reset_dialog")
        self.reset.set_default_size(150, 100)
        self.reset.set_title("Advertencia")
        self.accept = builder.get_object("aceptar_btn")
        self.cancel = builder.get_object("cancelar_btn")
        self.advert = builder.get_object("advertencia")
        
        self.reset.show_all()
